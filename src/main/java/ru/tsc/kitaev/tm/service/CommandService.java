package ru.tsc.kitaev.tm.service;

import ru.tsc.kitaev.tm.api.repository.ICommandRepository;
import ru.tsc.kitaev.tm.api.service.ICommandService;
import ru.tsc.kitaev.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
